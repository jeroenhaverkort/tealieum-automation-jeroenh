package userInterfaceTests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static helpers.BrowserFactory.Browser.CHROME;
import static helpers.BrowserFactory.createBrowser;

import static junit.framework.TestCase.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class JukePremiumTests {

    WebDriver driver;
    JavascriptExecutor javascriptExecutor;
    String jukePremiumWebsite = "https://abonnementen.juke.nl/premium?utm_source=juke-fm&utm_medium=lp-premium&utm_content=cta-premium";
    WebDriverWait wait;

    @Before
    public void setUp() {

        // Driver instantieren en browser openen
        driver = createBrowser(CHROME);

        // JS Executor aanmaken
        javascriptExecutor = (JavascriptExecutor) driver;

        // Wait aanmaken
        wait = new WebDriverWait(driver, 10);

        // Venster maximaliseren
        driver.manage().window().maximize();

        // Website bezoeken
        driver.get(jukePremiumWebsite);

        // Cookies setten (zie ook methode hieronder)
        wait.until(elementToBeClickable(By.cssSelector("[id='cookie-button']"))).click();

        // Pagina refreshen (moet na het zetten van de cookies)
        driver.navigate().refresh();
    }

    @After
    public void tearDown() {
        // Driver en browser sluiten
        driver.quit();
    }

    @Test
    public void jukePremiumFooterDataTest() {

        loginWithEmail();
        checkFooterData();
    }

    private void loginWithEmail() {

        wait.until(elementToBeClickable(By.cssSelector("[class='btn submit-button button_submit dynamic-button   ']"))).click();
        wait.until(elementToBeClickable(By.cssSelector("[id='emailInput']"))).sendKeys("nienke.glover@gmail.com");
        driver.findElement(By.cssSelector("[type='password']")).sendKeys("Glover1337");
        driver.findElement(By.cssSelector("[type='submit']")).click();
        wait.until(elementToBeClickable(By.xpath("(//a[@class='site-footer__link ng-binding ng-scope'])[1]")));
    }

    private void checkFooterData() {

        List<String> textList = new ArrayList<>();
        textList.add("Privacy Beleid");
        textList.add("Gebruiksvoorwaarden");
        textList.add("Veelgestelde vragen");
        textList.add("Contact");
        textList.add("Over JUKE");

        List<String> hrefList = new ArrayList<>();
        hrefList.add("https://privacy.talpanetwork.com/nl/privacy/");
        hrefList.add("https://privacy.talpanetwork.com/nl/voorwaarden/");
        hrefList.add("/juridisch/veelgestelde-vragen");
        hrefList.add("/juridisch/contact");
        hrefList.add("/juridisch/imprint");

        for(int i=1; i<6; i++) {
            System.out.println("Count is: " + i);

            String actualText = driver.findElement(By.xpath("(//a[@class='site-footer__link ng-binding ng-scope'])[" + i + "]")).getText();
            String actualAttribute = driver.findElement(By.xpath("(//a[@class='site-footer__link ng-binding ng-scope'])[" + i + "]")).getAttribute("href");

            System.out.println(actualText);
            System.out.println(actualAttribute);

            // Footer Text is correct
            assertTrue(actualText.contains(textList.get(i - 1)));

            // Footer links are correct
            assertTrue(actualAttribute.contains(hrefList.get(i - 1)));

            // No extra elements are present
            assertTrue(driver.findElements(By.xpath("(//a[@class='site-footer__link ng-binding ng-scope'])[" + (5+i) + "]")).isEmpty());
        }
    }
}
