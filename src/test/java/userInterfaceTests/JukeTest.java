package userInterfaceTests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import static helpers.BrowserFactory.Browser.CHROME;
import static helpers.BrowserFactory.createBrowser;

public class JukeTest {

    WebDriver driver;
    JavascriptExecutor javascriptExecutor;
    String jukeWebsite = "https://acc-radio.juke.nl/";
    WebDriverWait wait;

    @Before
    public void setUp() {

        // Driver instantieren en browser openen
        driver = createBrowser(CHROME);

        // JS Executor aanmaken
        javascriptExecutor = (JavascriptExecutor) driver;

        // Wait aanmaken
        wait = new WebDriverWait(driver, 10);

        // Venster maximaliseren
        driver.manage().window().maximize();

        // Website bezoeken
        driver.get(jukeWebsite);

        // Accepteer cookies
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[class='button button--primary']"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-automation-id='logo_home']")));

        // Cookies setten (zie ook methode hieronder)
        setCookies();

        // Pagina refreshen (moet na het zetten van de cookies)
        driver.navigate().refresh();

        // Taq EventLog laten laden
        Object result = javascriptExecutor.executeScript("return Taq.push(['load', 'EventLog'])");
//        System.out.println("Succeeded at loading EventLog: " + result.toString());
    }

    @After
    public void tearDown() {
        // Driver en browser sluiten
        driver.quit();
    }

    private void setCookies() {
        Cookie debugCookie = new Cookie("tealium-debug-cookie", "__TA_DEBUG__");
        Cookie consentCookie = new Cookie("talpa-radio_cookie-consent", "true");
        driver.manage().addCookie(debugCookie);
        driver.manage().addCookie(consentCookie);
    }
}
