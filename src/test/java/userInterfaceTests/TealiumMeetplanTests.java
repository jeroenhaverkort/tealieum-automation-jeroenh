package userInterfaceTests;

import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class TealiumMeetplanTests extends JukeTest {

    // *** TESTS *** //
    @Test
    public void eventLogMenuSearchTest() throws InterruptedException {

        useSearchBar("npo");

        checkEventLog(
                "search",
                "menu",
                "npo",
                "menu_search"
        );

    } // menu_search

    @Test
    public void eventLogForSelectingMenuItemsTest() throws InterruptedException {

        clickMenuItem("home");

        checkEventLog(
                "select",
                "menu",
                "home",
                "menu_select"
        );

        clickMenuItem("stations");

        checkEventLog(
                "select",
                "menu",
                "stations",
                "menu_select"
        );

        clickMenuItem("genres");

        checkEventLog(
                "select",
                "menu",
                "genres",
                "menu_select"
        );

        clickMenuItem("shows");

        checkEventLog(
                "select",
                "menu",
                "shows",
                "menu_select"
        );

        clickMenuItem("terugluisteren");

        checkEventLog(
                "select",
                "menu",
                "terugluisteren",
                "menu_select"
        );

        clickMenuItem("collection");

        checkEventLog(
                "select",
                "menu",
                "mijn collectie",
                "menu_select"
        );

        clickMenuItem("abonnementen");

        checkEventLog(
                "select",
                "menu",
                "abonnementen",
                "menu_select"
        );

        clickMenuItem("giftcard");

        checkEventLog(
                "select",
                "menu",
                "cadeaukaart",
                "menu_select"
        );

//        clickMenuItem("premium");
//
//        checkEventLog(
//                "juke-premium",
//                "cta",
//                "navigation",
//                "cta_juke-premium"
//        );

    } // menu_select(all_labels), cta_juke-premium(navigation)

    @Test
    public void eventLogForRegisteringWithEmailTest() throws InterruptedException {

        registerWithEmail();

        checkEventLog(
                "register",
                "gigya",
                "mail",
                "gigya_register"
        );
    } // gigya_register(email)

    @Test
    public void eventLogForRegisteringWithFacebookTest() throws InterruptedException {

        registerWithFacebook();

        checkEventLog(
                "register",
                "gigya",
                "facebook",
                "gigya_register"
        );
    } // gigya_register(facebook)

    @Test
    public void eventLogForLogInAndOutWithEmailTest() throws InterruptedException {

        loginWithEmail();

        checkEventLog(
                "login",
                "gigya",
                "mail",
                "gigya_login"
        );

        logoutFromJuke();

        checkEventLog(
                "logout",
                "gigya",
                "mail",
                "gigya_logout"
        );
    } // gigya_log-in(email), gigya_logout(email)

    @Test
    public void eventLogForLogInAndOutWithFacebookTest() throws InterruptedException {

        loginWithFacebook();

        checkEventLog(
                "login",
                "gigya",
                "facebook",
                "gigya_login"
        );

        logoutFromJuke();

        checkEventLog(
                "logout",
                "gigya",
                "facebook",
                "gigya_logout"
        );
    } // gigya_log-in(facebook), gigya_logout(facebook)

    @Test
    public void eventLogForPlayButtonTest() throws InterruptedException {

        clickStationPlayButton("538");

        checkEventLog(
                "start",
                "juke-player",
                "538",
                "juke-player_start"
        );

        checkEventLog(
                "play",
                "juke-player",
                "538",
                "juke-player_play"
        );
    } // juke-player_start, juke-player_play

    @Test
    public void eventLogForStopButtonTest() throws InterruptedException {

        clickStationPlayButton("radio-veronica");

        clickStationPlayButton("radio-veronica");

        checkEventLog(
                "stop",
                "juke-player",
                "radio veronica",
                "juke-player_stop"
        );
    } // juke-player_stop

    @Test
    public void eventLogForVolumeButtontest() throws InterruptedException {

        clickStationPlayButton("538");

        moveVolumeButton();

        checkEventLog(
                "volume",
                "juke-player",
                "volume",
                "juke-player_volume"
        );
    } // juke-player_volume

    @Test
    public void eventLogForAddingTrackToFavoritesTest() throws InterruptedException {

        loginWithEmail();

        clickStationPlayButton("538");

        addTrackToFavorites();

        checkEventLog(
                "add-my-category",
                "juke-player",
                "NOT_IMPORTANT",
                "juke-player_add-my-category"
        );

        addTrackToFavorites();

        checkEventLog(
                "remove-my-category",
                "juke-player",
                "NOT_IMPORTANT",
                "juke-player_remove-my-category"
        );
    } // juke-player_add-my-category, juke-player_remove-my-category

    @Test
    public void eventLogForExpandingContentInteractionTest() throws InterruptedException {

        clickExpandButton("538");

        checkEventLog(
                "expand",
                "content-interactions",
                "538",
                "content-interactions_expand"
        );
    } // content-interactions_expand

    @Test
    public void eventLogForExpandingAllContentInteractionsTest() throws InterruptedException {

        clickExpandAllButton();

        checkEventLog(
                "expand-all",
                "content-interactions",
                "populaire zenders",
                "content-interactions_expand-all"
        );

        clickCollapseButton();

        checkEventLog(
                "collapse",
                "content-interactions",
                "populaire zenders",
                "content-interactions_collapse"
        );
    } // content-interactions_expand-all, content-interactions_collapse

    @Test
    public void eventLogForAddingStationToFavoritesTest() throws InterruptedException {

        loginWithEmail();

        checkEventLog(
                "expand",
                "gigya",
                "log-in-button",
                "gigya_expand"
        );

        clickAddStationButton("538");

        checkEventLog(
                "add-my-category",
                "content-interactions",
                "538",
                "content-interactions_add-my-category"
        );

        clickAddStationButton("538");

        checkEventLog(
                "remove-my-category",
                "content-interactions",
                "538",
                "content-interactions_remove-my-category"
        );
    } // gigya_expand(log-in-button), content-interactions_add-my-category, content-interactions_remove-my-category

    @Test
    public void eventLogGigyaExpandWithoutLogginInTest() throws InterruptedException {

        clickAddStationButton("538");

        checkEventLog(
                "expand",
                "gigya",
                "add-my-category",
                "gigya_expand"
        );
    } // gigya_expand(add-my-category)

    @Test
    public void eventLogClickCtaJukePremiumButtonTest() throws InterruptedException {

        clickJukePremiumButton();

        checkEventLog(
                "juke-premium",
                "cta",
                "my-collection",
                "cta_juke-premium"
        );
    } // cta_juke-premium(my-collection)

    @Test
    public void eventLogClickSkipBackwardOnDemandTest() throws InterruptedException {

        clickMenuItem("terugluisteren");
        playOnDemandRadio();
        clickSkipButton(1);

        checkEventLog(
                "skip-backward",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_skip-backward"
        );

        clickSkipButton(2);

        checkEventLog(
                "skip-forward",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_skip-forward"
        );
    } // juke-player-on-demand_skip-backward, juke-player-on-demand_skip-forward

    @Test
    public void eventLogClickScrubberOnDemandTest() throws InterruptedException {

        clickMenuItem("terugluisteren");
        playOnDemandRadio();
        clickScrubber();

        checkEventLog(
                "scrubber",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_scrubber"
        );
    } // juke-player-on-demand_scrubber

    @Test
    public void eventLogPlayButtonOnDemand() throws InterruptedException {

        clickMenuItem("terugluisteren");
        playOnDemandRadio();

        Thread.sleep(5000);

        clickPlayButtonOnDemand();

        checkEventLog(
                "play",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_play"
        );

        checkEventLog(
                "start",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_start"
        );

        checkEventLog(
                "stop",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_stop"
        );

        checkEventLog(
                "resume",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_resume"
        );
    } //juke-player-on-demand_(play/start/stop/resume)


    @Test
    public void eventLogPlayOnDemandTrackTest() throws InterruptedException {

        loginWithEmail();
        clickMenuItem("collection");

        clickOnDemandTrack();

        checkEventLog(
                "play",
                "juke-player-on-demand",
                "NOT_IMPORTANT",
                "juke-player-on-demand_play"
        );
    }

    @Ignore
    @Test
    public void eventLogSocialShareTest() throws InterruptedException {

        driver.get(jukeWebsite + "radiozenders/538");

        clickShareButton("facebook");
        Thread.sleep(5000);
        driver.close(); //TODO: nog even fixen dat ie de popup sluit want nu doet ie moeilijk

        clickShareButton("twitter");
        Thread.sleep(5000);
        driver.close();
    }





    // *** METHODS *** //
    private void
    checkEventLog(String eventActionExpected, String eventCategoryExpected, String eventLabelExpected, String eventNameExpected) throws InterruptedException {

        // A sleep is required, because there is no way to assert if the events are loaded yet
        Thread.sleep(3000);

        System.out.println("\n****\nChecking event log:");

        // EventLog ophalen met JS script
//        ArrayList<HashMap<String, String>> resultingEventLog = (ArrayList<HashMap<String, String>>) javascriptExecutor.executeScript("return Taq.getExtension('EventLog').eventLog");
        Object resultingEventLogObject = javascriptExecutor.executeScript("return Taq.getExtension('EventLog').eventLog");

        if (resultingEventLogObject instanceof ArrayList<?>) {
            ArrayList<?> resultingEventLog = ((ArrayList<?>) resultingEventLogObject);

            System.out.println("EventLog contained " + resultingEventLog.size() + " arrays:");
            boolean isFirst = true;
            boolean eventFound = false;
            for (int i = resultingEventLog.size() - 1; i >= 0; i--) {
                HashMap<?, ?> eventArray = (HashMap<?, ?>) resultingEventLog.get(i);
                String eventName = (String) eventArray.get("event_name");
                if (eventName == null) {
                    System.out.println(i + ": Array was empty");
                } else {
                    System.out.println(i + ": " + eventName);
                }
                if (eventName != null && eventName.equals(eventNameExpected) && isFirst) {

                    String eventActionActual = (String) eventArray.get("event_action");
                    System.out.println("\tevent_action: " + eventActionActual);
                    Assertions.assertThat(eventActionActual).isEqualTo(eventActionExpected);

                    String eventCategoryActual = (String) eventArray.get("event_category");
                    System.out.println("\tevent_category: " + eventCategoryActual);
                    Assertions.assertThat(eventCategoryActual).isEqualTo(eventCategoryExpected);

                    if (!eventLabelExpected.equals("NOT_IMPORTANT")) {
                        String eventLabelActual = (String) eventArray.get("event_label");
                        System.out.println("\tevent_label: " + eventLabelActual);
                        Assertions.assertThat(eventLabelActual).isEqualTo(eventLabelExpected);
                    } else {
                        System.out.println("\tevent_label: skipped, noted as NOT_IMPORTANT");
                    }

                    String eventNameActual = (String) eventArray.get("event_name");
                    System.out.println("\tevent_name: " + eventNameActual);
                    Assertions.assertThat(eventNameActual).isEqualTo(eventNameExpected);

                    isFirst = false;
                    eventFound = true;
                }
            }
            if (!eventFound) {
                Assertions.fail("TEST RESULT: event '" + eventNameExpected + "' NOT launched");
            } else {
                System.out.println("TEST RESULT: event '" + eventNameExpected + "' is successfully launched");
            }
        }
    else {
        Assertions.fail("Returned object from javascript execution was not an expected ArrayList<?>");
    }
}

    private void useSearchBar(String searchRequest) {

        wait.until(elementToBeClickable(By.xpath("//input[@data-automation-id='search_input']"))).sendKeys(searchRequest);
    }

    private void clickStationPlayButton(String radioStationName) { // 538, sky-radio, radio-10, radio-veronica, qmusic, 100-nl, slam, bnr, npo-radio-1, npo-radio-2, npo-3fm, npo-radio-4, npo-radio-5, funx

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@data-automation-id='radio_" + radioStationName + "']//button[@data-automation-id='play']"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-automation-id='player']//button[@data-automation-id='play']")));
    }

    private void clickAddStationButton(String radioStationName) { // 538, sky-radio, radio-10, radio-veronica, qmusic, 100-nl, slam, bnr, npo-radio-1, npo-radio-2, npo-3fm, npo-radio-4, npo-radio-5, funx

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@data-automation-id='radio_" + radioStationName + "']//button[@data-automation-id='station_add']"))).click();
    }

    private void moveVolumeButton() {

        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='player_volume']"))).click();
        driver.findElement(By.xpath("//input[@data-automation-id='player_volumebar']")).click();
    }

    private void addTrackToFavorites() {

        wait.until(elementToBeClickable(By.xpath("//div[@data-automation-id='player_now_playing']//button[@data-automation-id='add_track']"))).click();
    }

    private void loginWithEmail() {

        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='menu_account']"))).click();
        wait.until(elementToBeClickable(By.xpath("//p[@class='have-account-redirect']/a"))).click();
        wait.until(elementToBeClickable(By.xpath("//input[@type='email']"))).sendKeys("nienke.glover@gmail.com");
        driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Glover1337");
        driver.findElement(By.xpath("//input[@type='submit']")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='gigya-screen-dialog-main']")));
    }

    private void loginWithFacebook() {

        // Store the current window handle
        String winHandleBefore = driver.getWindowHandle();

        // Perform the click operation that opens new window
        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='menu_account']"))).click();
        wait.until(elementToBeClickable(By.xpath("//p[@class='have-account-redirect']/a"))).click();
        wait.until(elementToBeClickable(By.xpath("//table[@class='gigya-login-providers-container']"))).click();

        // Switch to new window opened
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        // Perform the actions on new window
        wait.until(elementToBeClickable(By.xpath("//input[@name='email']"))).sendKeys("mees.carr@gmail.com");
        driver.findElement(By.xpath("//input[@name='pass']")).sendKeys("Carr1337");
        driver.findElement(By.xpath("//input[@value='Aanmelden']")).click();

        // Close the new window, if that window no more required
        //driver.close();

        // Switch back to original browser (first window)
        driver.switchTo().window(winHandleBefore);

        // Continue with original browser (first window)
    }

    private void logoutFromJuke() {

        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='menu_account']"))).click();
        wait.until(elementToBeClickable(By.xpath("//a[@onclick='gigya.accounts.logout();']"))).click();
    }

    private void clickMenuItem(String menu_item) { // home, stations, genres, shows, collection, premium

        driver.findElement(By.xpath("//a[@data-automation-id='menu_" + menu_item + "']")).click();
    }

    private void clickExpandButton(String station_tile) {

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@data-automation-id='radio_" + station_tile + "']//a[@data-automation-id='station_more']"))).click();
    }

    private void clickCollapseButton() {

        driver.findElement(By.xpath("//button[@data-automation-id='close_show_more']")).click();
    }

    private void clickExpandAllButton() {

        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='show_more_modal'][1]"))).click();
    }

    private void clickJukePremiumButton() {

        wait.until(elementToBeClickable(By.xpath("//div[@data-automation-id='Playlists']/..//a[@target='_blank']"))).click();
    }

    private void removeConsentCookieAndAgreeAgain() {

        driver.manage().deleteCookieNamed("talpa-radio_cookie-consent");
        driver.navigate().refresh();

        Object result = javascriptExecutor.executeScript("return Taq.push(['load', 'EventLog'])");
//        System.out.println("Succeeded at loading EventLog: " + result.toString());

        wait.until(elementToBeClickable(By.xpath("//button[@type='PRIMARY']"))).click();
    }

    private void registerWithEmail() {

        Random rand = new Random();
        int randomNumber = rand.nextInt(1000000) + 1;

        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='menu_account']"))).click();
        wait.until(elementToBeClickable(By.xpath("//input[@aria-label='Email']"))).sendKeys("testaccount_" + randomNumber + "@gmail.com");
        driver.findElement(By.xpath("//input[@aria-label='First name']")).sendKeys("Test");
        driver.findElement(By.xpath("//input[@aria-label='Last name']")).sendKeys("Account");
        driver.findElement(By.xpath("//input[@aria-label='Password']")).sendKeys("Wachtwoord123");
        driver.findElement(By.xpath("//input[@aria-label='Confirm password']")).sendKeys("Wachtwoord123");
        driver.findElement(By.xpath("//input[@class='gigya-input-radio']")).click();
//        driver.findElement(By.xpath("//input[@name='data.terms_talpa_accepted']")).click();
        driver.findElement(By.xpath("//input[@value='Akkoord en registreren']")).click();


    }

    private void registerWithFacebook() {

        // Store the current window handle
        String winHandleBefore = driver.getWindowHandle();

        // Perform the click operation that opens new window
        wait.until(elementToBeClickable(By.xpath("//button[@data-automation-id='menu_account']"))).click();

        wait.until(presenceOfElementLocated(By.xpath("//button[@aria-label='Facebook']"))).click();

        // Switch to new window opened
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        // Perform the actions on new window
        wait.until(elementToBeClickable(By.xpath("//input[@name='email']"))).sendKeys("mees.carr@gmail.com");

        driver.findElement(By.xpath("//input[@name='pass']")).sendKeys("Carr1337");

        driver.findElement(By.xpath("//input[@value='Aanmelden']")).click();

        // Close the new window, if that window no more required
        //driver.close(); // not required because window closes automatically

        // Switch back to original browser (first window)
        driver.switchTo().window(winHandleBefore);

        // Continue with original browser (first window)
    }

    private void playOnDemandRadio() {

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@data-automation-id,'on_demand_clip')]//button[@data-automation-id='play']"))).click();
    }

    private void clickSkipButton(int i) throws InterruptedException { // i = 1 skip back, i = 2 skip forward

        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[@data-automation-id='player_skip'])[" + i + "]"))).click();
        Thread.sleep(2000);

    }

    private void clickScrubber() {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@aria-label='player controls']/div/div/div/div"))).click();
    }

    private void clickPlayButtonOnDemand() throws InterruptedException {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-automation-id='player']//button[@data-automation-id='play']"))).click();
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-automation-id='player']//button[@data-automation-id='play']"))).click();

    }

    private void clickOnDemandTrack() {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-automation-id='track']"))).click();
    }

    private void clickShareButton(String medium) {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[id='share_" + medium + "']"))).click();

    }

}
