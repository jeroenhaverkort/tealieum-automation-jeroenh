package helpers;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.logging.Level;

public class BrowserFactory {

    public enum Browser {
        CHROME,
        FIREFOX
    }

    public static WebDriver createBrowser(Browser browser) {
        switch (browser) {
            case CHROME:
                return createChromeBrowser();
            default:
                return createFireFoxBrowser();
        }
    }

    private static EventFiringWebDriver createFireFoxBrowser() {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.INFO);

        FirefoxDriverManager.getInstance().setup();

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        return webDriverWithListener(new FirefoxDriver(new FirefoxOptions()));
    }

    private static WebDriver createChromeBrowser() {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.INFO);

        ChromeDriverManager.getInstance().setup();

        // Toegevoegd: stukje om de log preferences te zetten
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);

        return webDriverWithListener(new ChromeDriver(chromeOptions));
    }

    private static EventFiringWebDriver webDriverWithListener(WebDriver driver) {
        return new EventFiringWebDriver(driver).register(new BrowserEventListener());
    }

}
