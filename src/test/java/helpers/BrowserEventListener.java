package helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class BrowserEventListener implements WebDriverEventListener {

    public void afterChangeValueOf(WebElement driver, WebDriver arg1) {
        // No need for implementation yet
    }

    public void afterClickOn(WebElement element, WebDriver driver) {
        // No need for implementation yet
    }

    public void afterFindBy(By arg0, WebElement arg1, WebDriver arg2) {
        // No need for implementation yet
    }

    public void afterNavigateBack(WebDriver arg0) {
        // No need for implementation yet
    }

    public void afterNavigateForward(WebDriver arg0) {
        // No need for implementation yet
    }

    public void afterNavigateTo(String arg0, WebDriver arg1) {
        // No need for implementation yet
    }

    public void afterScript(String arg0, WebDriver arg1) {
        // No need for implementation yet
    }

    public void beforeChangeValueOf(WebElement element, WebDriver driver) {
        highlightElement(element, driver);
    }

    public void beforeClickOn(WebElement element, WebDriver driver) {
        highlightElement(element, driver);
    }

    private void highlightElement(WebElement element, WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
                "color: red; border: 2px solid red;");
    }

    public void beforeFindBy(By arg0, WebElement arg1, WebDriver arg2) {
        // No need for implementation yet
    }

    public void beforeNavigateBack(WebDriver arg0) {
        // No need for implementation yet
    }

    public void beforeNavigateForward(WebDriver arg0) {
        // No need for implementation yet
    }

    public void beforeNavigateTo(String arg0, WebDriver arg1) {
        // No need for implementation yet
    }

    public void beforeScript(String arg0, WebDriver arg1) {
        // No need for implementation yet
    }

    public void onException(Throwable throwable, WebDriver driver) {
        // No need for implementation yet
    }

    @Override
    public <X> void beforeGetScreenshotAs(OutputType<X> outputType) {

    }

    @Override
    public <X> void afterGetScreenshotAs(OutputType<X> outputType, X x) {

    }

    public void afterNavigateRefresh(WebDriver arg0) {
        // No need for implementation yet
    }

    public void beforeNavigateRefresh(WebDriver arg0) {
        // No need for implementation yet
    }

    public void afterChangeValueOf(WebElement arg0, WebDriver arg1, CharSequence[] arg2) {
        // No need for implementation yet
    }

    public void beforeChangeValueOf(WebElement arg0, WebDriver arg1, CharSequence[] arg2) {
        // No need for implementation yet
    }

    public void beforeAlertAccept(WebDriver driver) {
        // No need for implementation yet
    }

    public void afterAlertAccept(WebDriver driver) {
        // No need for implementation yet
    }

    public void afterAlertDismiss(WebDriver driver) {
        // No need for implementation yet
    }

    public void beforeAlertDismiss(WebDriver driver) {
        // No need for implementation yet
    }

    public void afterSwitchToWindow(String arg0, WebDriver arg1) {
        // No need for implementation yet
    }

    public void beforeSwitchToWindow(String arg0, WebDriver arg1) {
        // No need for implementation yet
    }

}

